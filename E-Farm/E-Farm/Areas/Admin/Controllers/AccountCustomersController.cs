﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using E_Farm.Areas.Admin.Models;

namespace E_Farm.Areas.Admin.Controllers
{
    public class AccountCustomersController : Controller
    {
        private DataEntities db = new DataEntities();

        // GET: Admin/AccountCustomers
        public ActionResult Index()
        {
            return View(db.AccountCustomer.ToList());
        }

        // GET: Admin/AccountCustomers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountCustomer accountCustomer = db.AccountCustomer.Find(id);
            if (accountCustomer == null)
            {
                return HttpNotFound();
            }
            return View(accountCustomer);
        }

        // GET: Admin/AccountCustomers/Create
        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CustomerId,Fullname,Gender,Address,Phone,Email,Username,Password,Picture,CreateDate")] AccountCustomer accountCustomer)
        {
            if (ModelState.IsValid)
            {
                db.AccountCustomer.Add(accountCustomer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(accountCustomer);
        }

        // GET: Admin/AccountCustomers/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountCustomer accountCustomer = db.AccountCustomer.Find(id);
            if (accountCustomer == null)
            {
                return HttpNotFound();
            }
            return View(accountCustomer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AccountCustomer accountCustomer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(accountCustomer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(accountCustomer);
        }

        // GET: Admin/AccountCustomers/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountCustomer accountCustomer = db.AccountCustomer.Find(id);
            if (accountCustomer == null)
            {
                return HttpNotFound();
            }
            db.AccountCustomer.Remove(accountCustomer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
    }
}
