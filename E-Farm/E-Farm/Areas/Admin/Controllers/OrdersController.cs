﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using E_Farm.Areas.Admin.Models;
using PagedList;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Data.SqlClient;

namespace E_Farm.Areas.Admin.Controllers
{
    public class OrdersController : Controller
    {
        private DataEntities db = new DataEntities();

        public ActionResult Index(int? pageNo)
        {
            int pageSize = 5;
            int pageNumber = pageNo ?? 1;
            var orders = db.Orders.Include(o => o.AccountCustomer).Include(o => o.Payment).Include(o => o.Transport).ToList();
            return View(orders.ToPagedList(pageNumber, pageSize));
        }

        //public ActionResult Index(int? pageNo)
        //{
        //    var od = db.Database.SqlQuery<OrderInformation>("EXEC usp_getOrderDetail").ToList();
        //    int pageSize = 5;
        //    int pageNumber = pageNo ?? 1;
        //    return View(od.ToPagedList(pageNumber, pageSize));
        //}

        public ActionResult Details(string id)
        {
            
            if (id == null)
            {
                return HttpNotFound();
            }
            var order = db.Database.SqlQuery<OrderInformation>
                ("SELECT o.OrderId as 'OrderId' , c.Fullname AS 'CustomerName', c.[Address] as 'Address', p.Name as 'PaymentName', t.Name as 'TransferName', pr.Name as 'ProductName',od.Quantity as 'Quantity' , od.TotalPrice as 'Price', o.TotalPrice as 'TotalPrice', o.Status as 'Status' "
                + " from [Order] o " 
                + " inner join AccountCustomer c ON c.CustomerId = o.CustomerId " 
                + " inner join Transport t ON t.TranId = o.TranId " 
                + " inner join Payment p ON p.PayId = o.PayId " 
                + " inner join OrderDetail od ON od.OrderId = o.OrderId " 
                + " inner join Product pr ON pr.ProductId = od.ProductId " 
                + " where o.OrderId = @orderId ", new SqlParameter("@orderId", id)).ToList();
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        public ActionResult Details2(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Order order = db.Orders.Include(o => o.AccountCustomer).Include(o => o.Payment).Include(o => o.Transport).FirstOrDefault(x => x.OrderId == id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(db.AccountCustomer, "CustomerId", "Fullname", order.CustomerId);
            ViewBag.PayId = new SelectList(db.Payments, "PayId", "Name", order.PayId);
            ViewBag.TranId = new SelectList(db.Transports, "TranId", "Name", order.TranId);
            return View(order);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Order order)
        {
            Order o = db.Orders.Find(order.OrderId);
            if(o != null)
            {
                o.Status = order.Status;
                db.SaveChanges();

                var lst = db.Orders.Include(x => x.AccountCustomer).Where(x => x.Status == false);
                Session["Order"] = lst;
                Session["QuantityOrder"] = lst.Count();
                return RedirectToAction("Index");
            }          
            return View(order);
        }

        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Order order = new Order();

            if (order == null)
            {
                return HttpNotFound();
            }

            if (order.OrderDetails.Count > 0)
            {
                TempData["errMessage"] = "<div class='alert alert-danger'><strong>Cảnh báo! </strong>Không thể xóa đơn hàng</div>";
                return RedirectToAction("Index");
            }
            
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult selectOrderUnconfimred()
        {
            return null;
        }

        public ActionResult ExportToExcel(string id)
        {
            var gv = new GridView();
            gv.DataSource = db.Database.SqlQuery<OrderInformation>
                ("SELECT o.OrderId as 'OrderId' , c.Fullname AS 'CustomerName', c.[Address] as 'Address', p.Name as 'PaymentName', t.Name as 'TransferName', pr.Name as 'ProductName',od.Quantity as 'Quantity' , od.TotalPrice as 'Price', o.TotalPrice as 'TotalPrice', o.Status as 'Status' "
                + " from [Order] o "
                + " inner join AccountCustomer c ON c.CustomerId = o.CustomerId "
                + " inner join Transport t ON t.TranId = o.TranId "
                + " inner join Payment p ON p.PayId = o.PayId "
                + " inner join OrderDetail od ON od.OrderId = o.OrderId "
                + " inner join Product pr ON pr.ProductId = od.ProductId "
                + " where o.OrderId = @orderId ", new SqlParameter("@orderId", id)).ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Hoadon.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "utf-8";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            return View("Index");
        }


    }
}
