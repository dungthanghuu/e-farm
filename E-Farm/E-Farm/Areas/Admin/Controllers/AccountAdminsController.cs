﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using E_Farm.Areas.Admin.Models;

namespace E_Farm.Areas.Admin.Controllers
{
    public class AccountAdminsController : Controller
    {
        private DataEntities db = new DataEntities();
        
        public ActionResult Index()
        {
            return View(db.AccountAdmins.ToList());
        }
        
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountAdmin accountAdmin = db.AccountAdmins.Find(id);
            if (accountAdmin == null)
            {
                return HttpNotFound();
            }
            return View(accountAdmin);
        }
        
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(string username, AccountAdmin accountAdmin, HttpPostedFileBase UploadAvtAdmin)
        {

            var check = db.AccountAdmins.SingleOrDefault(x => x.Username == accountAdmin.Username);
            if (check != null)
            {
                TempData["errMessage"] = "<div class='alert alert-danger'><strong>Cảnh báo! </strong>Tên đăng nhập đã tồn tại.</div>";
                return View(accountAdmin);
            }

            if (ModelState.IsValid)
            {
                if (UploadAvtAdmin != null)
                {
                    UploadAvtAdmin.SaveAs(Server.MapPath("~/Areas/Admin/Content/imgAccAdmin/") + UploadAvtAdmin.FileName);
                    accountAdmin.Picture = "/Areas/Admin/Content/imgAccAdmin/" + UploadAvtAdmin.FileName;
                }

                Guid guid = Guid.NewGuid();
                accountAdmin.AccAdminId = guid.ToString();
                accountAdmin.CreateDate = @DateTime.Now;
                accountAdmin.Password = accountAdmin.Password.ToMD5();
                db.AccountAdmins.Add(accountAdmin);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(accountAdmin);
        }
        
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            AccountAdmin accountAdmin = db.AccountAdmins.Find(id);
            if (accountAdmin == null)
            {
                return HttpNotFound();
            }
            return View(accountAdmin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AccountAdmin accountAdmin, HttpPostedFileBase UploadAvtAdmin)
        {

            if (ModelState.IsValid)
            {
                if (UploadAvtAdmin != null)
                {
                    UploadAvtAdmin.SaveAs(Server.MapPath("~/Areas/Admin/Content/imgAccAdmin/") + UploadAvtAdmin.FileName);
                    accountAdmin.Picture = "/Areas/Admin/Content/imgAccAdmin/" + UploadAvtAdmin.FileName;
                }
                accountAdmin.Password = accountAdmin.Password.ToMD5();
                db.Entry(accountAdmin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(accountAdmin);
        }
        
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountAdmin accountAdmin = db.AccountAdmins.Find(id);
            if (accountAdmin == null)
            {
                return HttpNotFound();
            }
            db.AccountAdmins.Remove(accountAdmin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
    }
}
