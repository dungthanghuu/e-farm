﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using E_Farm.Areas.Admin.Models;

namespace E_Farm.Areas.Admin.Controllers
{
    public class PaymentsController : Controller
    {
        private DataEntities db = new DataEntities();
      
        public ActionResult Index()
        {
            return View(db.Payments.ToList());
        }

       
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment payment = db.Payments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            return View(payment);
        }

        
        public ActionResult Create()
        {
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Payment payment)
        {
            var checkName = db.Payments.SingleOrDefault(x => x.Name == payment.Name);
            if (checkName != null)
            {
                TempData["errMessage"] = "<div class='alert alert-danger'><strong>Cảnh báo! </strong>Tên hình thức thanh toán đã tồn tại.</div>";
                return View(payment);
            }

            if (ModelState.IsValid)
            {
                db.Payments.Add(payment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payment);
        }

       
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment payment = db.Payments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            return View(payment);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Payment payment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payment);
        }

      
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment payment = db.Payments.Find(id);

            if (payment == null)
            {
                return HttpNotFound();
            }

            if (payment.Orders.Count > 0) {
                TempData["errMessage"] = "<div class='alert alert-danger'><strong>Cảnh báo! </strong>Không thể xóa hình thức thanh toán " + payment.Name + " vì đang tồn tại trong Order.</div>";
                return RedirectToAction("Index");
            }

            db.Payments.Remove(payment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
    }
}
