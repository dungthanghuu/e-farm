﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using E_Farm.Areas.Admin.Models;

namespace E_Farm.Areas.Admin.Controllers
{
    public class CategoriesController : Controller
    {
        private DataEntities db = new DataEntities();

        // GET: Admin/Categories
        public ActionResult Index()
        {
            return View(db.Categorys.ToList());
        }

        // GET: Admin/Categories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Category category = db.Categorys.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }

        // GET: Admin/Categories/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Category category)
        {
            var checkName = db.Categorys.FirstOrDefault(x => x.Name == category.Name);
            if (checkName != null)
            {
                TempData["errMessage"] = "<div class='alert alert-danger'><strong>Cảnh báo! </strong>Tên danh mục đã tồn tại.</div>";
                return View(category);
            }

            if (ModelState.IsValid)
            {
                db.Categorys.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Category category = db.Categorys.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            return View(category);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Category category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        public ActionResult Delete(int? id)
        {

            if (id == null)
            {
                return HttpNotFound();
            }

            Category category = db.Categorys.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }

            if (category.Products.Count > 0)
            {
                TempData["errMessage"] = "<div class='alert alert-danger'><strong>Cảnh báo! </strong>Không thể xóa danh mục " + category.Name + " vì đang tồn tại sản phẩm thuộc danh mục.</div>";
                return RedirectToAction("Index");
            }

            db.Categorys.Remove(category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}
