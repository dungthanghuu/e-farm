﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using E_Farm.Areas.Admin.Models;
using PagedList;
namespace E_Farm.Areas.Admin.Controllers
{
    public class ProductsController : Controller
    {
        private DataEntities db = new DataEntities();

        public ActionResult Index(string search, int? pageNo)
        {
            if (search == null)
            {
                search = "";
            }
            int pageSize = 5;
            int pageNumber = pageNo ?? 1;
            return View(db.Products.Include(x => x.Category).Where(x => x.Name.Contains(search)).ToList().ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Product product = db.Products.Include(o => o.Category).FirstOrDefault(x => x.ProductId == id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        public PartialViewResult Search(string name, int? pageNo)
        {
            int pageSize = 5;
            int pageNumber = pageNo ?? 1;
            var data = db.Products.Include(b => b.Category).Where(x => x.Name.Contains(name)).ToList();
            return PartialView(data.ToPagedList(pageNumber,pageSize));
        }

        public ActionResult Create()
        {
            ViewBag.CateId = new SelectList(db.Categorys, "CateId", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Product product, HttpPostedFileBase UploadImgProducts)
        {
            var checkName = db.Products.SingleOrDefault(x => x.Name == product.Name);
            if (checkName != null)
            {
                TempData["errMessage"] = "<div class='alert alert-danger'><strong>Cảnh báo! </strong>Sản phẩm đã tồn tại trong kho</div>";
                return RedirectToAction("Index");
            }
            if (ModelState.IsValid)
            {
                if (UploadImgProducts != null)
                {
                    UploadImgProducts.SaveAs(Server.MapPath("~/Areas/Admin/Content/imagesProducts/") + UploadImgProducts.FileName);
                    product.Picture = "/Areas/Admin/Content/imagesProducts/" + UploadImgProducts.FileName;
                }
                product.CreateDate = DateTime.Now;
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CateId = new SelectList(db.Categorys, "CateId", "Name", product.CateId);
            return View(product);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.CateId = new SelectList(db.Categorys, "CateId", "Name", product.CateId);
            return View(product);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Product product, HttpPostedFileBase UploadImgProducts)
        {
            if (ModelState.IsValid)
            {
                if (UploadImgProducts != null)
                {
                    UploadImgProducts.SaveAs(Server.MapPath("~/Areas/Admin/Content/imagesProducts/") + UploadImgProducts.FileName);
                    product.Picture = "/Areas/Admin/Content/imagesProducts/" + UploadImgProducts.FileName;
                }
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CateId = new SelectList(db.Categorys, "CateId", "Name", product.CateId);
            return View(product);
        }

    }
}
