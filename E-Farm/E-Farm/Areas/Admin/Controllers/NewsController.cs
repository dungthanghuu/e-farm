﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using E_Farm.Areas.Admin.Models;

namespace E_Farm.Areas.Admin.Controllers
{
    public class NewsController : Controller
    {
        private DataEntities db = new DataEntities();

        public ActionResult Index()
        {
            return View(db.New.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.New.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        public ActionResult Create()
        {
            return View();
        }
     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HttpPostedFileBase UploatImgNews ,News news)
        {
            if (ModelState.IsValid)
            {
                if(UploatImgNews != null)
                {
                    UploatImgNews.SaveAs(Server.MapPath("~/Areas/Admin/Content/imgNews/") + UploatImgNews.FileName);
                    news.Picture = "/Areas/Admin/Content/imgNews/" + UploatImgNews.FileName;
                }
                news.CreateDate = DateTime.Now;
                db.New.Add(news);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(news);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.New.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(News news, HttpPostedFileBase UploatImgNews)
        {
            if (ModelState.IsValid)
            {
                if (UploatImgNews != null)
                {
                    UploatImgNews.SaveAs(Server.MapPath("~/Areas/Admin/Content/imgNews/") + UploatImgNews.FileName);
                    news.Picture = "/Areas/Admin/Content/imgNews/" + UploatImgNews.FileName;
                }

                db.Entry(news).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(news);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.New.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            db.New.Remove(news);
            db.SaveChanges();
            return RedirectToAction("Index");
        }                
    }
}
