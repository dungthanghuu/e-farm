﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using E_Farm.Areas.Admin.Models;
using PagedList;

namespace E_Farm.Areas.Admin.Controllers
{
    public class OrderDetailsController : Controller
    {
        private DataEntities db = new DataEntities();

        public ActionResult Index(int? pageNo)
        {
            var od = db.Database.SqlQuery<GetOrder>("EXEC usp_getOrder").ToList();

            int pageSize = 5;
            int pageNumber = pageNo ?? 1;

            return View(od.ToPagedList(pageNumber,pageSize));
        }

        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            OrderDetail orderDetail = db.OrderDetails.Include(o => o.Product).Include(o => o.Order).FirstOrDefault(x => x.OrderDetailId == id);
            if (orderDetail == null)
            {
                return HttpNotFound();
            }
            return View(orderDetail);
        }

        public ActionResult ViewOrderAll()
        {
            return View();
        }
    }
}
