﻿using E_Farm.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;

namespace E_Farm.Areas.Admin.Controllers
{
    public class SystemController : Controller
    {

        private DataEntities db = new DataEntities();

        public ActionResult Index()
        {
            var lst = db.Orders.Include(a => a.AccountCustomer).Where(x => x.Status == false).OrderByDescending(x => x.CreateDate);
            Session["Order"] = lst;
            int count = lst.Count();
            Session["QuantityOrder"] = count;

            return View();
        }

        public ActionResult CheckOrder()
        {
            var lst = db.Orders.Include(a => a.AccountCustomer).Where(x => x.Status == false);
            Session["Order"] = lst;
            Session["QuantityOrder"] = lst.Count();

            return null;
        }

        public ActionResult Login()
        {
            return View();
        }

        // Đăng nhập
        [HttpPost]
        public ActionResult Login(string username, string user_password)
        {
            string pass = user_password.ToMD5();
            var account = db.AccountAdmins.SingleOrDefault(x => x.Username == username && x.Password == pass);
            if (account != null)
            {
                // Gán Session lưu thông tin đăng nhập
                Session["IdAdmin"] = account.AccAdminId;
                Session["FullnameAdmin"] = account.Fullname;
                Session["AvatarAdmin"] = account.Picture;
                return RedirectToAction("Index");
            }
            else
            {
                // Thông báo lỗi khi người dùng nhập không đúng
                TempData["msg"] = "<div class='alert-danger'>Username or Password invalid</div>";
                return View();
            }
        }

        // Đăng xuất
        public ActionResult Logout()
        {
            // Xóa Session lưu thông tin đăng nhập
            Session["FullnameAdmin"] = null;
            Session["AvatarAdmin"] = null;
            Session["IdAdmin"] = null;
            return RedirectToAction("Login");
        }

    }
}