﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    [Table("AccountCustomer")]
    public class AccountCustomer
    {
        [Key]
        [StringLength(36)]
        [Display(Name = "Mã khách hàng")]
        public string CustomerId { get; set; }

        [Required(ErrorMessage = "Mời nhập tên")]
        [StringLength(126)]
        [Display(Name = "Họ và tên")]
        public string Fullname { get; set; }

        [Required(ErrorMessage = "Mời chọn giới tính")]
        [Display(Name = "Giới tính")]
        public bool Gender { get; set; }

        [Required(ErrorMessage = "Mời nhập địa chỉ")]
        [StringLength(264)]
        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Mời nhập số điện thoại")]
        [StringLength(10, ErrorMessage = "Số điện thoại không được quá 10 ký tự")]
        [Display(Name = "Số điện thoại")]
        [RegularExpression("^03|09[0-9]{8}$", ErrorMessage = "Số điện thoại chỉ được nhập số và cần nhập đúng định dạng 03 hoặc 09")]
        public string Phone { get; set; }

        [StringLength(64)]
        [DataType(DataType.EmailAddress, ErrorMessage = "Sai cú pháp email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mời nhập tên đăng nhập")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Tên đăng nhập chỉ chứa số và chữ")]
        [StringLength(maximumLength: 26, MinimumLength = 8, ErrorMessage = "Tên đăng nhập phải từ 8 đến 26 ký tự")]
        [Display(Name = "Tên đăng nhập")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Mời nhập password")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Tên đăng nhập chỉ chứa số và chữ")]
        [StringLength(maximumLength: 36, MinimumLength = 6, ErrorMessage = "Mật khẩu phải từ 6 đến 26 ký tự")]
        [Display(Name = "Mật khẩu")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [StringLength(200)]
        [Display(Name = "Ảnh")]
        public string Picture { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }
    }
}
