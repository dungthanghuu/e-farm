﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    [Table("CommentProduct")]
    public class CommentProduct
    {
        [Key]
        [Display(Name = "Mã đánh giá")]
        public int CommentId { get; set; }

        [Required]
        [Display(Name = "Tên người đánh giá")]
        [StringLength(128)]
        public string Name { get; set; }

        [ForeignKey("Product")]
        [Display(Name = "Mã sản phẩm")]
        public int ProductId { get; set; }


        [Display(Name = "Nội dung đánh giá")]
        [Column(TypeName = "ntext")]
        public string Comment { get; set; }

        public Product Product { get; set; }
    }
}