﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    [Table("Transport")]
    public class Transport
    {
        [Key]
        [Display(Name = "Mã hình thức vận chuyển")]
        public int TranId { get; set; }

        [Display(Name = "Tên hình thức vận chuyển")]
        [Required(ErrorMessage = "Mời nhập tên hình thức vận chuyển")]
        [StringLength(264)]
        public string Name { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}