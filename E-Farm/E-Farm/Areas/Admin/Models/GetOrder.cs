﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    public class GetOrder
    {
        public string OrderId { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string Payment { get; set; }
        public string Transport { get; set; }
        public string Product { get; set; }
        public int Quantity { get; set; }
        public float TotalPrice { get; set; }
        public bool Status { get; set; }
    }
}