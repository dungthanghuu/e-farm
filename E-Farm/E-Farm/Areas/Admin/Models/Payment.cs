﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    [Table("Payment")]
    public class Payment
    {
        [Key]
        [Display(Name = "Mã hình thức thanh toán")]
        public int PayId { get; set; }

        [Required(ErrorMessage = "Mời nhập tên hình thức thanh toán")]
        [Display(Name = "Tên hình thức thanh toán")]
        [StringLength(264)]
        public string Name { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}