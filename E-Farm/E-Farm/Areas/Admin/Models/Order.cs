﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    [Table("Order")]
    public class Order
    {
        [Key]
        [StringLength(36)]
        [Display(Name = "Mã đặt hàng")]
        public string OrderId { get; set; }

        [ForeignKey("AccountCustomer")]
        [StringLength(36)]
        [Display(Name = "Mã khách hàng")]
        public string CustomerId { get; set; }

        [Display(Name = "Tổng tiền")]
        public float TotalPrice { get; set; }

        [ForeignKey("Payment")]
        [Required(ErrorMessage = "Mời chọn hình thức thanh toán")]
        [Display(Name = "Hình thức thanh toán")]
        public int PayId { get; set; }

        [ForeignKey("Transport")]
        [Required(ErrorMessage = "Mời chọn hình thức vận chuyển")]
        [Display(Name = "Hình thức vận chuyển")]
        public int TranId { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Trạng thái")]
        public bool Status { get; set; }

        public AccountCustomer AccountCustomer { get; set; }

        public Payment Payment { get; set; }

        public Transport Transport { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}