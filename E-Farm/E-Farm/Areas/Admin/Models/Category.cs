﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    [Table("Category")]
    public class Category
    {
        [Key]
        [Display(Name = "Mã danh mục")]
        public int CateId { get; set; }

        [Display(Name = "Ký hiệu danh mục")]
        [StringLength(128)]
        [Required(ErrorMessage = "Mời nhập ký hiệu danh mục")]
        public string Symbol { get; set; }

        [Display(Name = "Tên danh mục")]
        [Required(ErrorMessage = "Mời nhập tên danh mục")]
        [StringLength(128)]
        public string Name { get; set; }

        [Display(Name = "Đơn vị tính")]
        [Required(ErrorMessage = "Mời đơn vị tính")]
        [StringLength(64)]
        public string Unit { get; set; }

        public virtual ICollection<Product> Products { get; set; }
        
    }
}