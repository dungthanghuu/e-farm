﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    public class DataEntities :DbContext
    {
        public DataEntities() : base("name=DataConection")
        {
        }

        public DbSet<AccountAdmin> AccountAdmins { get; set; }
        public DbSet<AccountCustomer> AccountCustomer { get; set; }
        public DbSet<Category> Categorys { get; set; }
        public DbSet<News> New { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Transport> Transports { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<CommentProduct> CommentProducts { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }

    }
}