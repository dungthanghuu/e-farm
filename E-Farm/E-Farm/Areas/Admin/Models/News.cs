﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Farm.Areas.Admin.Models
{
    [Table("News")]
    public class News
    {
        [Key]
        [Display(Name = "Mã tin tức")]
        public int NewsId { get; set; }

        [StringLength(350)]
        [Display(Name = "Tiêu đề")]
        public string Title { get; set; }

        [Display(Name = "Nội dung")]
        [AllowHtml]
        [Column(TypeName = "ntext")]
        public string Content { get; set; }

        [StringLength(264)]
        [Display(Name = "Ảnh")]
        public string Picture { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }
    }
}