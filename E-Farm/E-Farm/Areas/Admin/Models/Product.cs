﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Farm.Areas.Admin.Models
{
    [Table("Product")]
    public class Product
    {
        [Key]
        [Display(Name = "Mã sản phẩm")]
        public int ProductId { get; set; }

        [ForeignKey("Category")]
        [Required(ErrorMessage = "Mời chọn danh mục")]
        [Display(Name = "Danh mục")]
        public int CateId { get; set; }

        [Required(ErrorMessage = "Mời nhập tên sản phẩm")]
        [Display(Name = "Tên sản phẩm")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Mời nhập số lượng")]
        [Display(Name = "Số lượng")]
        [Range(0,100000,ErrorMessage = "Lỗi nhập số lượng")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Mời nhập giá")]
        [Display(Name = "Giá")]
        [Range(0, 1000000000, ErrorMessage = "Lỗi nhập giá")]
        public float Price { get; set; }

        [Display(Name = "Giá khuyến mại")]
        [Range(0, 1000000000, ErrorMessage = "Lỗi nhập giá khuyến mãi")]
        public float? PriceDiscount { get; set; }

        [Display(Name = "Ảnh")]
        public string Picture { get; set; }

        [Display(Name = "Mô tả")]
        [AllowHtml]
        public string Description { get; set; }
        
        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Trạng thái")]
        public bool Status { get; set; }

        public Category Category { get; set; }

        public virtual ICollection<CommentProduct> CommentProducts { get; set; }

        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}