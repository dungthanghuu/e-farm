﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    [Table("OrderDetail")]
    public class OrderDetail
    {
        [Key]
        [StringLength(36)]
        [Display(Name = "Mã đơn hàng")]
        public string OrderDetailId { get; set; }

        [ForeignKey("Product")]
        [Display(Name = "Tên sản phẩm")]
        public int ProductId { get; set; }

        [ForeignKey("Order")]
        [StringLength(36)]
        [Display(Name = "Mã hóa đơn")]
        public string OrderId { get; set; }

        [Display(Name = "Số lượng")]
        public int Quantity { get; set; }

        [Display(Name = "Thành tiền")]
        public float TotalPrice { get; set; }

        public Product Product { get; set; }

        public Order Order { get; set; }
        
    }
}