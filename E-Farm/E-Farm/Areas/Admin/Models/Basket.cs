﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    public class Basket
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public string Picture { get; set; }
        public int Quantity { get; set; }
        public float TotalPrice { get; set; }
    }
}