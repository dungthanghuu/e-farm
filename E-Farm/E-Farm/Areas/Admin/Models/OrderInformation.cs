﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    public class OrderInformation
    {
        public string OrderId { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string PaymentName { get; set; }
        public string TransferName { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public float Price { get; set; }
        public float TotalPrice { get; set; }
        public bool Status { get; set; }
    }
}