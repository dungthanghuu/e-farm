﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace E_Farm.Areas.Admin.Models
{
    [Table("AccountAdmin")]
    public class AccountAdmin
    {
        [Key]
        [Display(Name = "Mã admin")]
        [StringLength(36)]
        public string AccAdminId { get; set; }
        
        [Display(Name = "Họ và tên")]
        [StringLength(126)]
        [Required(ErrorMessage = "Mời nhập tên")]
        [RegularExpression("[A-Za-z ]*", ErrorMessage = "Tên không hợp lệ")]
        public string Fullname { get; set; }

        [Display(Name = "Tên đăng nhập")]
        [StringLength(maximumLength: 26, MinimumLength = 8, ErrorMessage = "Tên đăng nhập phải từ 8 đến 26 ký tự")]
        [Required(ErrorMessage = "Mời nhập tên đăng nhập")]
        public string Username { get; set; }

        [Display(Name = "Mật khẩu")]
        [RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Mật khẩu chỉ chứa số và chữ")]
        [StringLength(maximumLength:36, MinimumLength = 8, ErrorMessage = "Mật khẩu phải từ 8 đến 26 ký tự")]
        [Required(ErrorMessage = "Mời nhập mật khẩu")]
        public string Password { get; set; }

        [Display(Name = "Ảnh")]
        [StringLength(200)]
        public string Picture { get; set; }

        [Display(Name = "Ngày tạo")]
        public DateTime CreateDate { get; set; }
    }
}