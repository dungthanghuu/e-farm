/* NUGET: BEGIN LICENSE TEXT
 *
 * Microsoft grants you the right to use these script files for the sole
 * purpose of either: (i) interacting through your browser with the Microsoft
 * website or online service, subject to the applicable licensing or use
 * terms; or (ii) using the files as included with a Microsoft product subject
 * to that product's license terms. Microsoft reserves all other rights to the
 * files not expressly granted by Microsoft, whether by implication, estoppel
 * or otherwise. Insofar as a script file is dual licensed under GPL,
 * Microsoft neither took the code under GPL nor distributes it thereunder but
 * under the terms set out in this paragraph. All notices and licenses
 * below are for informational purposes only.
 *
 * NUGET: END LICENSE TEXT */
/*!
** Unobtrusive validation support library for jQuery and jQuery Validate
** Copyright (C) Microsoft Corporation. All rights reserved.
*/

/*jslint white: true, browser: true, onevar: true, undef: true, nomen: true, eqeqeq: true, plusplus: true, bitwise: true, regexp: true, newcap: true, immed: true, strict: false */
/*global document: false, jQuery: false */

(function ($) {
    var $jQval = $.validator,
        adapters,
        data_validation = "unobtrusiveValidation";

    function setValidationValues(options, ruleName, value) {
        options.rules[ruleName] = value;
        if (options.message) {
            options.messages[ruleName] = options.message;
        }
    }

    function splitAndTrim(value) {
        return value.replace(/^\s+|\s+$/g, "").split(/\s*,\s*/g);
    }

    function escapeAttributeValue(value) {
        // As mentioned on http://api.jquery.com/category/selectors/
        return value.replace(/([!"#$%&'()*+,./:;<=>?@\[\\\]^`{|}~])/g, "\\$1");
    }

    function getModelPrefix(fieldName) {
        return fieldName.substr(0, fieldName.lastIndexOf(".") + 1);
    }

    function appendModelPrefix(value, prefix) {
        if (value.indexOf("*.") === 0) {
            value = value.replace("*.", prefix);
        }
        return value;
    }

    function onError(error, inputElement) {  // 'this' is the form element
        var container = $(this).find("[data-valmsg-for='" + escapeAttributeValue(inputElement[0].name) + "']"),
            replaceAttrValue = container.attr("data-valmsg-replace"),
            replace = replaceAttrValue ? $.parseJSON(replaceAttrValue) !== false : null;

        container.removeClass("field-validation-valid").addClass("field-validation-error");
        error.data("unobtrusiveContainer", container);

        if (replace) {
            container.empty();
            error.removeClass("input-validation-error").appendTo(container);
        }
        else {
            error.hide();
        }
    }

    function onErrors(event, validator) {  // 'this' is the form element
        var container = $(this).find("[data-valmsg-summary=true]"),
            list = container.find("ul");

        if (list && list.length && validator.errorList.length) {
            list.empty();
            container.addClass("validation-summary-errors").removeClass("validation-summary-valid");

            $.each(validator.errorList, function () {
                $("<li />").html(this.message).appendTo(list);
            });
        }
    }

    function onSuccess(error) {  // 'this' is the form element
        var container = error.data("unobtrusiveContainer"),
            replaceAttrValue = container.attr("data-valmsg-replace"),
            replace = replaceAttrValue ? $.parseJSON(replaceAttrValue) : null;

        if (container) {
            container.addClass("field-validation-valid").removeClass("field-validation-error");
            error.removeData("unobtrusiveContainer");

            if (replace) {
                container.empty();
            }
        }
    }

    function onReset(event) {  // 'this' is the form element
        var $form = $(this),
            key = '__jquery_unobtrusive_validation_form_reset';
        if ($form.data(key)) {
            return;
        }
        // Set a flag that indicates we're currently resetting the form.
        $form.data(key, true);
        try {
            $form.data("validator").resetForm();
        } finally {
            $form.removeData(key);
        }

        $form.find(".validation-summary-errors")
            .addClass("validation-summary-valid")
            .removeClass("validation-summary-errors");
        $form.find(".field-validation-error")
            .addClass("field-validation-valid")
            .removeClass("field-validation-error")
            .removeData("unobtrusiveContainer")
            .find(">*")  // If we were using valmsg-replace, get the underlying error
                .removeData("unobtrusiveContainer");
    }

    function validationInfo(form) {
        var $form = $(form),
            result = $form.data(data_validation),
            onResetProxy = $.proxy(onReset, form),
            defaultOptions = $jQval.unobtrusive.options || {},
            execInContext = function (name, args) {
                var func = defaultOptions[name];
                func && $.isFunction(func) && func.apply(form, args);
            }

        if (!result) {
            result = {
                options: {  // options structure passed to jQuery Validate's validate() method
                    errorClass: defaultOptions.errorClass || "input-validation-error",
                    errorElement: defaultOptions.errorElement || "span",
                    errorPlacement: function () {
                        onError.apply(form, arguments);
                        execInContext("errorPlacement", arguments);
                    },
                    invalidHandler: function () {
                        onErrors.apply(form, arguments);
                        execInContext("invalidHandler", arguments);
                    },
                    messages: {},
                    rules: {},
                    success: function () {
                        onSuccess.apply(form, arguments);
                        execInContext("success", arguments);
                    }
                },
                attachValidation: function () {
                    $form
                        .off("reset." + data_validation, onResetProxy)
                        .on("reset." + data_validation, onResetProxy)
                        .validate(this.options);
                },
                validate: function () {  // a validation function that is called by unobtrusive Ajax
                    $form.validate();
                    return $form.valid();
                }
            };
            $form.data(data_validation, result);
        }

        return result;
    }

    $jQval.unobtrusive = {
        adapters: [],

        parseElement: function (element, skipAttach) {
            /// <summary>
            /// Parses a single HTML element for unobtrusive validation attributes.
            /// </summary>
            /// <param name="element" domElement="true">The HTML element to be parsed.</param>
            /// <param name="skipAttach" type="Boolean">[Optional] true to skip attaching the
            /// validation to the form. If parsing just this single element, you should specify true.
            /// If parsing several elements, you should specify false, and manually attach the validation
            /// to the form when you are finished. The default is false.</param>
            var $element = $(element),
                form = $element.parents("form")[0],
                valInfo, rules, messages;

            if (!form) {  // Cannot do client-side validation without a form
                return;
            }

            valInfo = validationInfo(form);
            valInfo.options.rules[element.name] = rules = {};
            valInfo.options.messages[element.name] = messages = {};

            $.each(this.adapters, function () {
                var prefix = "data-val-" + this.name<W!	C��I��I�\U��w9<G�z�͍|B�!wqd��ߋ�61�+o�5�ȝt�ƁrVMn�|�j�6�5�=������@�P�HYI�ɫE�<��\K���kчok����7��n�#"�^ȷ[�X�s���4����6�^�I�(��s�D�A����ώ� �-}�:��G5FK����ţ�2YI�|֯�$q�Ɯ"4�n��|�(��=�VgZ4�߼���^���,Ié+��xYO�	��1v�ῧ#�'��Y�	+��5˶I1�e5�OZ��������f� Kӷ�%��mR�bǇ����<i���_�]��z����8�Ne��p(T���F֓��JY)�?�Ż1[n<��O�4$�2��������A�l��HI�Q���]T�J�-���[\?��-�9�^�wwe���'��b!�\��Zx2�^9��c�1��I+= ��l��Q E!孝/��U�Ym^L��xFp��B�.LG�X/���Y�_󊳣ŔMT��j�+O��e����QI���˯�����q��M�|0�6�ji'����,B;���;���}Ɯ��۴ڏ6ku݊9�w=ͩ���+_j�Bi)����j�&;jOm$���^�r٢^œws��g8F����VEk�x&�y��}�(I.>��KԵ��CK@0>���S�~�U��] ⴲ��/��&���L��m��v��P�o�)��A|c��m�)=&�k���ý�������s|eVia������u�� s[�1F}�o	�=�m�QD���qH�h�!T�*\��a���b|��5G�E����0\�lK:������+���Y�T�1),|��
��S�}��5W���JR�6b�5+k��H%�B�3!�mcS��k������5J���/���=�M����Q|���>��[��"�Ԓif����H׼�9���4��˄y�j|0�!��J�������.���W+X���O�z�,m����~#Kb/��'���K{K�'A������\6�$B�g&W��qq�����,�	5��@׵��z��hX�N�@�AO��en����'���Ң��As��6&̌����.�R����:ͧI�i��y�����]���s'�C�c��L�M�Ϳ��5;�� +��\0�cE�,��0�īk��t�� a(R4ꇰ*�P�5*�,^#�܅4M����(:�K�XKK4 ���H[U�S�(Ŝ>�e|l�Z��Xj�����}4�L��s(�e��R�򪁇w4^c����U�#c�!B��+���a��|U�k��)�S�5e����"�K]q�k��ԩ4�@V�K���ʿA'5��u�ٕ�,�u�g<R
o��rn���E�k7z�C�@4��=��ژ�J.��g���\ă�����CD�Go�Yh@�&�ío��Ƃ8[3?R�l�1�G^�{�޶�D���T����.�*m�K� 1�_٧L���9�s����W)煔383@αz@�B��p��ʩ閆��~{�tBi��v�x�F������"xƟ��݇5�2��?B]:��ww�	�7�T�e���-��R:Ӧ�\���|����I\o �͡�?�]
�_����d,�j_;�~t���4�E8��E�sC���B�v�rnBENO�mCD���)�,"�&���te�C�/UkHZ8��{�̨��H���I)��*i'��UIm�R��Gay_���xZw�]#6G����5���~��E3L;�-��\�M�KZbrƣg�J;+���FB�|�<�i�A���\�j�&�;@�<r�m�+iVD�|����Z
Yp7�UGY��*T�m �=�6Y]6�V*�}Ij�*Ȧ����^*��*���=f�G�y��$�P�	�\Ha&BY��׶3����Y%�A��-l�J���ȩXy��l�G�ʟ������	�]�L�����QYP�V��rҰ���y�+�iN�h�����X�K��R�]�_�Wܔ��(�)�����E��b3	x,���pY�X0[����dlJB<q��NBdb�/�z*Jy���v��3�LU>y�%j��ͻ��(R�:��9�Xж4qᮠS¡���5�����#|��h��ӡ��L����`R�]Z�[�>�_+e��7��&P��v ���HW&��0@�?� *ԋ��9'�!؂h��s��]ݏ~lj!�*W�#����^�����l����<2K�=�h�0z�h�S����4Y��a둱�՘t��#�$(�4�DD#(`8?�f�`? t@@d����>�:J����u��Y��:tXno�F؜p���2����|q'�3c�M,�@e9����|��c.����0�Ul �+ų����%
����s2.� Lav��?%��qr^�O� ��*��1	ۼ���	9�@k����������tߺ5ө�� ���+F����"��J�=�0������	�W����E"ȏbX������8��[��#��
ӂH\TZ���4��'�^��]ti�p�_夐��p	uޢ�{ZD��{�#�¢���n�.�S}W� ��׾e�Sғj8j��k��pȼF�Ű;'��nq�ƾ����u���:e_~F��L�����6�+DIR���xN��������e������+ɬ��z{	u�\���1��G�Wv����ߓ��>�f޵M�s��_�Pg�@˺}��`T#�O����`:�+�S�0�ϕ����J-m,\L$(Ԩ? ��6QYr��������((h������3���� ���I������X���~>�!4������\x[��� -�¤��7����楽4SUh�y�c~�*~C�V�N�Z?�YsX�#����/���"t�� ����	,ʝ��߶��K_ ��?�k�8���@4��$���>v)�!񪡆�����1V���9�����?(�4�F��ʌ�-1؏���-M����k�Z�E��0������a��
Q�����&D��
-2pV�2��	e��b�T|Zbj��I0!1k�pܲ�i�"�j�� Ǘ,,-3���-�^�n�,GU)q/��S�>43�/�E{�L��뀭���q��Lj�'�c�N�a�(݁Hq�<�A��4_��xA��屠�@��?D�T�d�8��qoU	�A�3�A��T-p�����]p�{�ND�|O�v���6FŤ���[-�Kd�&rHm��Ǳ��a/1p͇� ���ò�ǗNܞ��p����)#�ՑxGv�Sv��sEC��>&���<Zګ�0�����	}�|m�q�w-��}�C���R��[2�,��Ԛ<H�P�9����gZ�e% �'�4�h���ȼ��H3Mu�:e��zU��8p>�\z�k/���iPc{��-�����5a��r׼ ���/O��C�B"�)�s��@�5cM �h�u,����fT�p���f��~������_a�";�j�����n O3�U�A��9v����w�ZUo ��I��<��g��Å��r1�x̦u� �a�HyFTD��F����9k���5��代_ӧ��s���|��c�Ȩ/6�����j�G%�������_�������}+���T�g��P�7é��̉l����6�pKTKx�W�L�y�6{kT��]pO���5���m"<Ea`>�#|DK��fpN�xQ
�<"pjT�
�����b���i4@+����@�B8�wc@訄�!ef��$� ��v����$�oTj�&}1ç\�=�O�����ᖉ�ȇx��]�d�hF�$�M�xIK�=c]���^L{�
��2?��v��>k�59�
����do�ۏ�g%�˾��V�m��Ѻ ]2R{L*�@�Ҥjӫsؕy�:�[]>ڵũ�w ����A��^�U�S@��=��=$�#*����՝yGr�:��r�H��-�is���v�&4��-��E2g��{��궄�|����S�����@��Z�-հ�;�~�q��#g���ȁD��lxBy5���*1�2�o�`r��ehv�
s-iU�*O�z����|��Jd~ˤ�k��^�r洨�]9��A��v�G%��Y����R�O�f�	�pQc�	�[W;� ������)����Q^�A���[��-��Ga&Q.{�����bG˟������4�B�}C��F����q9CǴa��6��y��������HO(���`� �*�P�����4�<ا�P�?E˽#�0x��4?��Ѥjj��_�?F0�ݮ*��r�j7�&�� ��H��@H�e��9���lM��PY�?ͣ�6�!�#)�2������t��ȍ�/��矓c'��r�ű�a[0]
n����ū�q����~WQ������9���(e���Bt_�/�^���ΐQDV�n�,/��}Bpp�/w��A�ӄT>_J�Y�RP1:�2=���J���b�ɟ����
L�q��c���Ꮱn��^��.�	'F�U]��3��Vt��~��"K�d�9j�wÔ|s���:}�����������j���/A�i�>A�i��Ød�J�0��)ܓ|�]�3�;�ϫ�W.HAJ���j�����~�SxXf'd�j|�r%�)k�~������4��%G!E�y�Pm�q6w��d�:����u<9����"�+{N�#'X���m�h ������YKJց��&�W�da�عEO� #�u=��B�t�c�ẕ��6�y�����K*j��-9�3	���}��Ǜ�iG���I�`��8�[�n�	:�c�L�@W襘��LnkD�l�Cˠ��,C������ ɭu��5=�	�������d���j�6��C`������uvҀ����x�nh�g 9vmאt6���?E�$F��W��2����b����o������(Dհ�<u�:$���˥(\Ʃ�/ˑ� ���	�z�JMk}�&BQܪ���`*��m�nA�}���AF�߆Q͂yv}� ��h�i�o�AN\�PM�����$��cX��gS���Ց2�ٵ��Y384��U`�*nR��~��<b翯�g{�;��=�O:��}�?���U	sG6�#
�A��z3�vz�dr(bC��)��\�;�I6���L��&K/��P�Z ͤ�I;A�q�WG�u8I<�w�<�	�|V�B�l��X��>��̞��^����e����G�ؑ��x��_��,������֜��ޠ�Ю}Axb��c����ۅ'J����w8�s����d</]���I��`j�z��It؋J��R�s�'�wѢ�WR��_�� ��|s��C8�>����� ���� ��w����Y�(ᖆ��I�[V��)k|2���}�Q�Nz�#k�?I=j_���@Ħ5�G^oeU��+1�
�	�3���q��}D�`m���_o.�gqoz�PK�V�k���$���|p��e>�`���*�����ehP�0R�CJ�>Y�QEE�"��~vތ���[�	`s~�1K�Y��U_��))�2��e��1�)��Fe�/����%p*s�-
�-iv��#��8m�E���|�YT��_�Kc�b��5�n)���8h(vT5����_���zz�'v�W�쾩�4((�P��ߗ:Ǥ]��뤉9�m���팔���M�	g�G�R���&I�;@�u��+���qQ��g��1�u�<X{�=%u����_#Y�HS�nmF��J��-ǿ�t��
L�~0��/&�ì�6f�c1⚹�+�Of�ޅ����R��L����	��7��tQDqwz�)�����s�i�b�_�݈�;劰�$��)��A�Xg��5����<�F���7 �9�����nWR��pE5�/�ߴn<��[��?����I�T�����S��f-p)�8\Ϣmق��{p��]J��fih���/ׯ�P �'�AֽN�-����ԃ#
�C}&Dl:b�#Ԣ�C�O��o�ec�H�]�Y�Evp���~_۱��t�����'��|!E6�|tH����7Խ5,���"=ގG��΀\9��k���)�5[��U��!GT�I�G�:��� 6��`�6h��\4�-/u�fw0\m�~�W�qx��v�	F����9a/���o�8��mg��EHG��Ν�����I��=ȟ>;%J�e6�^m�S�gŭma�ye��i}�i��B��c�W���g�_� ~FPĠ��������H�=�ݚ��!>�Ćze-��}�#p�0���qUI�!|������.�M�B����A�����яO�Ƃӂφ���U��ݷQ��b�%N�S����'�BJ�������s����g�����z�z
������I�O� 3<�$��u𝚂�y��q�B*۬�C�I��u\�z�<ݽ�^B��Mœ�����ƥ�,x
��f`ʂP�-�t��IO��}W��M#�W`�� ��I����h��4Kq���q��eUG8|�*پ��\�#�烉�]�oyq��~�i1l��-�ބ�ؖ�����ؼ�/�)�7�.g��ju?̻�~. �������Z��p;|�\%b{ 0�2�5pmЉto+���θߥ��[Q�1Z4�/ܡ��M�Jp����vK�(�Jh�ɸ�a�x%&���6K�{}��+�8����1j�u|������@^���Oc�B�������
.�E�.񽳯�s&��Մ��0�g���<=�#���E�X��ǲ���,WmV�c�����(]�y���=#�h,)�^qҽ⚪�r.�]��T)�h�&��r.7ea+ٞ��D�.h��"�*K�eЊ5�,$P��π�Kݣ]_�5r��j��'90>M� 3���і�/{>	����ͼ'ɠms��� h �dЭ�U���<Mf�FӤ�VPi��U��r!z�	�������������
-�c�+H���Bk[�r!�D�����*ЁNZ-?�k���c���(��+"A\�����.CQ��Wd��
�{MY�����Q�&%>	2eLː����֠������c�mC
ݾ8(OG,�q�Z�Qp[��'*m�Iey���m ��w8�o�n��ĩD�p���l2�;/�@�B79�a>T��wf�#��L�V��y��r�ҳ1*�E
=�
�� x]��c�Ko�?Jt-�ONa�Wbv�#"\�٤�}�/h�\��XKL�Ƭ��n{��Rm�������� Z����R&���)�s��Z���:�Q%TN�UW�{Q;���32�q����*���IQ�(S��D��^o�n��pd�,�b�`�D:�%B�BJ������z&wcp7�r��b�}��H)�pV��:z��t����$��\��o���\&�<������ߊ,�7��?*�_�̐r�a��_���.��^�1B׊X��N����p���<�+�M&����^5eh�GR�5Z��	�P'B��0�� K�:�7��?��gDlX���e���\ߝ��C(���A����.����{���v�q�ps� �m�Dv��
��s/n��fN8�6�R���-��;�u�EX��pq/�/��ux��X%�ę�Zg��|.�h�V�t��k��Q��%Y�{�h
m_5����e�K��5Kۈ���sc�o�ڗ��~F��M�O;V]�c�E��By��55��pԙW��[+�UV������%~X�U�`D���8S���g�	#[���T��L����q>ֱFSm[��D��baj��Y��n�����΢P>�Q\:��\����b���:2P�=c��)@8h/ C�t߈^W��u�q��>�*�L�{���$����ŗb�)Mc:@�����/8-�ә+}.(��N��0���z�%8��F/`�(m��,��2Iv&����
9�q!yrd��:����V#�]gvOՇ�/\5�!�pC��#͌ޫ}�!�\H�}u��?��4�.m�w5�{fL�"���"6�#(4<f��ѷR���N�[�I�l�aZN̼�T���dv��H��/��s5&\�[)j�pm��&I,�I
��.��(m���uOc�Ю���Z{��f���["('7ϸ*U[�W�2K-t'�vN�s�^g� Q3����3^s�u�gGK��?�̯��<�|L�Z�gD:#d�@�d�4P�R+|�瘐�	��+O�WY֦=�ɫ-R��>�j�AIqU�=����ѫ-	~�g�_M��c^c�	+y���"[��T�YI�&DwG3=n��&�"�����_�ݑ�9���Ʈro@�kɩ�����j�r}+��I��i��D �j��s1J����;;â_؄/���2�v)�J����ڿL&:I�(g���s(<��E�A�
�:�dn�)@%������T�=c-^Hn��S��nՅD۪�Dh'���[T^�&6�Y�^�$�5Ն@ �vS�G�E�.c���ROVT�,�TFT�*~�sp�r��	��_g��%?sP�&tm q�q����|��Zf.����Q�Ff2��y�ᨱU܀����u��jV�����,L<{��x�W�{*�9���ϓ�z���������ϭ�'r�K���i���e���-f�Y��>O!"�Nю�h�5���h��պW"�#��VT��[K�jW�,Mذ���d��9~
R��7���m�����>�>�)P���j)��W��㝷��.M�A�tT?{v��W
pk�IF�y�*���LgU�R�v���nE��[�_k�"B����=~�����y�)D�D0$WG��\����mѱ�6eqz��jA��ղ�1䩬C�Y
_�eW��HV6��l'>��Q�|9�C��������~�����6����r�M��j4 ���Q��@�1��z��I:}W'trߞ�)��@�j*��ۋ.����7�$���p�iS� �1%4��	-�F����н��$�Q�:����,�|�۩�g3��M/�@��j��rIJ���'�Ϫ��j���%� �_��0����P�o��@&@$0K� �?�k:�Y�9D@���V�D�}%����q�'����[0�w|����L���g�8�m3����J�Xwy
��ή�yp��w{���X�ʑs[�CUq��ի������Z�L��ŕ�� r-��[9D ��5�X������8�C�qh ��rKVJcY>�T�f=��0�}7����YO48��V-ߚ��.+4�d6�9w���-+�W� �`&MN� pkr�:�{�����]��CS�Y�[�Yn�*���pow�}��<ö�
,8GlK�^�/�L�q������J�K|c����t��H'#}�`�t��'�O�#���¹ByF켭����Ic�o\ �ϑc����?�)��ĵ�y����"V�{���\�����)"�P4�XS�#���b�"�<uMX�E,"�#�͖>[>�ݖU���c���8?�߾�{����e��B�㌣#�4���JEE��51`�q/Ԋ����Y��).����l�0�t�vJaz��R�t��VN�S��W�+��k��⭰�	`����i�f	��Դ��z3���aƵ�k~�a��H��ʝ.[��-[�y
`c&g����|�{��������ja���K�(V��hP2�?C�N�5�vo{����	8`� ��G�c}^QHq�����-�I"R��M׆|�w~p�������7�1��m�b�m7��䋍>ڀ+�fW������W�j�t��s-!��
*�hqMz"7�_3�UF�^�㋇��Z����B����c���x��;�|4ȥ!ܠ.�Cf�:!�Z�A@Cr:\�[+_�rN���uzς��8ٳp�[�$ b]t��O)��%��~��ئx}�q8���ǌ�	")������{�l��<�p�;��B���/[�
V�����:$,֠����.�U�|�w�H��
K�@_�.l�E_P)tn��<@�oq:�40W�"<��;#X�B��h���`��4��>AV�h�h:����]eD!���n�r�#�!���6u_�q%��1�@�K�cǽbI,v9���#��������2���ʠ�_B?Gi��85����X<h-<�3�0D����0�����q�� K�����:-����H���0z�q�O��.Ō��-�����]Cn(:=��4��|9[��>��L�JM9wF,G��D�d�O�L29���&�E����@��㽄�N��%~��%�Y1�$V`������#�-�K���|����gF&@\��Cx�hD���	S��/��\�m^�	����r{[erQ
�*X����n��c�;*��hO8�)��mb��T�7�Z�.�v�6^#�)�t�ߘO��E��趌�'PKe�dO����"S���'���)vN��28Q��%;�ttb:j��k� �Q�q�M1�-��VdFd� �� XgW��Ė���Q�����ڨ*=N���fyS!֪d�W�QՎ���;ǘ�c0둕Ph��&��L����b�:�lJ}�WQDp '�1t[��uaT����EV}]�2���z�ե���	�~*X�2$D��xi#&��:�w����>�1���'v�;��1�M>?�\"���f
��u0�Q�"ʹ���'`����d���/�.�T�7b3�M/�s��`J-�� +i��6���)S�xo#&���a�B�Yw?i�� ���&Q��k�e&�S.(�6o�b�A �����C҉R^�-�J�D� b���
�2wX*���~:�Pl����+�;��S�#��b����B���.�K�R��.0$W埮	\6:g� C� �k�zl�1�p��]Q[�"��v�a� ���X����L�v����Z�p5���:l�@wɷ<%1��aͭ)"bAxj��d��:�ɜ:QmY��L5u�A�.).�eN�a)��&ț`Q�k���7�ʸȾ�g�Tm��K%f����p������@�D�LEQ�|�BKKD�z��H���1�9%^f������H?�Qu���]GS<��j�7�䯌�eT����k��\d�n��e]�%��rێ�Y�{<���ď��Ϛ�;j��SX,A<>�ס��<���r�JZ�y�qj��$F�ߵ��>�z��p�?uȼ��dNb�+<�5e"�\��z�'��X�4�0R�N0������si8�Xr+
��
ϫ�J�TyR�>�5���D��(o&+�
�
' &uĸQ�C�E�IH�������`�w�(RP��g�S1M�J<"���(N��k>�ċN�QNTm�Z婖VWLA^�*����T�-�/�/ s�r�R!�g�?�"�7���?�*�o�+[;����I�%i���eR�,Z��H�vH[ڲE3�b�O@