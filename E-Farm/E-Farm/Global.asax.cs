﻿using E_Farm.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace E_Farm
{
    public class MvcApplication : System.Web.HttpApplication
    {
        
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Session_Start()
        {
            // thông tin admin
            Session["FullnameAdmin"] = null;
            Session["AvatarAdmin"] = null;
            Session["IdAdmin"] = null;

            Session["QuantityOrder"] = 0;
            Session["Order"] = new List<Order>();

            // thông tin khách hàng
            Session["login"] = 0;
            Session["CustomerId"] = null;
            Session["CustomerName"] = null;
            Session["CustomerPhone"] = null;
            Session["CustomerAddress"] = null;

            // Gio hang
            Session["Basket"] = new List<Basket>();
            Session["TotalQuantity"] = 0;
            Session["TotalPrice"] = 0;
            Session["Category"] = null;
            Session["Top8Product"] = null;

        }
        protected void Session_End()
        {
            // thông tin admin
            Session["FullnameAdmin"] = null;
            Session["AvatarAdmin"] = null;
            Session["IdAdmin"] = null;

            Session["TotalOrder"] = 0;

            // thông tin khách hàng
            Session["login"] = 0;
            Session["CustomerId"] = null;
            Session["CustomerName"] = null;
            Session["CustomerPhone"] = null;
            Session["CustomerAddress"] = null;

            // giỏ hàng
            Session["Basket"] = null;
            Session["TotalQuantity"] = 0;
            Session["TotalPrice"] = 0;
        }
    }
}
