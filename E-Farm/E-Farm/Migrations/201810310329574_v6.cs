namespace E_Farm.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AccountAdmin", "Password", c => c.String(nullable: false, maxLength: 36));
            AlterColumn("dbo.AccountCustomer", "Password", c => c.String(nullable: false, maxLength: 36));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AccountCustomer", "Password", c => c.String(nullable: false, maxLength: 26));
            AlterColumn("dbo.AccountAdmin", "Password", c => c.String(nullable: false, maxLength: 26));
        }
    }
}
