namespace E_Farm.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountAdmin",
                c => new
                    {
                        AccAdminId = c.String(nullable: false, maxLength: 36),
                        Fullname = c.String(nullable: false, maxLength: 126),
                        Username = c.String(nullable: false, maxLength: 26),
                        Password = c.String(nullable: false, maxLength: 26),
                        Picture = c.String(maxLength: 200),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AccAdminId);
            
            CreateTable(
                "dbo.AccountCustomer",
                c => new
                    {
                        CustomerId = c.String(nullable: false, maxLength: 36),
                        Fullname = c.String(nullable: false, maxLength: 126),
                        Gender = c.Boolean(nullable: false),
                        Address = c.String(nullable: false, maxLength: 264),
                        Phone = c.String(nullable: false, maxLength: 10),
                        Email = c.String(maxLength: 64),
                        Username = c.String(nullable: false, maxLength: 26),
                        Password = c.String(nullable: false, maxLength: 26),
                        Picture = c.String(maxLength: 200),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.Category",
                c => new
                    {
                        CateId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        Unit = c.String(nullable: false, maxLength: 64),
                    })
                .PrimaryKey(t => t.CateId);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        CateId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Quantity = c.Int(nullable: false),
                        Price = c.Single(nullable: false),
                        PriceDiscount = c.Single(),
                        Picture = c.String(),
                        Description = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Category", t => t.CateId, cascadeDelete: true)
                .Index(t => t.CateId);
            
            CreateTable(
                "dbo.CommentProduct",
                c => new
                    {
                        CommentId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 128),
                        ProductId = c.Int(nullable: false),
                        Comment = c.String(storeType: "ntext"),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.OrderDetail",
                c => new
                    {
                        OrderDetailId = c.String(nullable: false, maxLength: 36),
                        ProductId = c.Int(nullable: false),
                        OrderId = c.String(maxLength: 36),
                        Quantity = c.Int(nullable: false),
                        TotalPrice = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.OrderDetailId)
                .ForeignKey("dbo.Order", t => t.OrderId)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.Order",
                c => new
                    {
                        OrderId = c.String(nullable: false, maxLength: 36),
                        CustomerId = c.String(maxLength: 36),
                        TotalPrice = c.Single(nullable: false),
                        PayId = c.Int(nullable: false),
                        TranId = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.OrderId)
                .ForeignKey("dbo.AccountCustomer", t => t.CustomerId)
                .ForeignKey("dbo.Payment", t => t.PayId, cascadeDelete: true)
                .ForeignKey("dbo.Transport", t => t.TranId, cascadeDelete: true)
                .Index(t => t.CustomerId)
                .Index(t => t.PayId)
                .Index(t => t.TranId);
            
            CreateTable(
                "dbo.Payment",
                c => new
                    {
                        PayId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 264),
                    })
                .PrimaryKey(t => t.PayId);
            
            CreateTable(
                "dbo.Transport",
                c => new
                    {
                        TranId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 264),
                    })
                .PrimaryKey(t => t.TranId);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        NewsId = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 350),
                        Content = c.String(storeType: "ntext"),
                        Picture = c.String(maxLength: 264),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.NewsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderDetail", "ProductId", "dbo.Product");
            DropForeignKey("dbo.OrderDetail", "OrderId", "dbo.Order");
            DropForeignKey("dbo.Order", "TranId", "dbo.Transport");
            DropForeignKey("dbo.Order", "PayId", "dbo.Payment");
            DropForeignKey("dbo.Order", "CustomerId", "dbo.AccountCustomer");
            DropForeignKey("dbo.CommentProduct", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Product", "CateId", "dbo.Category");
            DropIndex("dbo.Order", new[] { "TranId" });
            DropIndex("dbo.Order", new[] { "PayId" });
            DropIndex("dbo.Order", new[] { "CustomerId" });
            DropIndex("dbo.OrderDetail", new[] { "OrderId" });
            DropIndex("dbo.OrderDetail", new[] { "ProductId" });
            DropIndex("dbo.CommentProduct", new[] { "ProductId" });
            DropIndex("dbo.Product", new[] { "CateId" });
            DropTable("dbo.News");
            DropTable("dbo.Transport");
            DropTable("dbo.Payment");
            DropTable("dbo.Order");
            DropTable("dbo.OrderDetail");
            DropTable("dbo.CommentProduct");
            DropTable("dbo.Product");
            DropTable("dbo.Category");
            DropTable("dbo.AccountCustomer");
            DropTable("dbo.AccountAdmin");
        }
    }
}
