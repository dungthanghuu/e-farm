﻿using E_Farm.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Farm.Controllers
{
    public class CommentProductController : Controller
    {
        DataEntities db = new DataEntities();

        public ActionResult AddComment(int Id,string Name, string ContentComment)
        {
            CommentProduct comment = new CommentProduct();
            comment.Name = Name;
            comment.Comment = ContentComment;
            comment.ProductId = Id;
            db.CommentProducts.Add(comment);
            int total = db.SaveChanges();

            if (total > 0)
            {
                return Content("Cám ơn quý khách đã đánh sản phẩm");
            }
            else
            {
                return Content("Đã xảy ra lỗi");
            }
        }
    }
}