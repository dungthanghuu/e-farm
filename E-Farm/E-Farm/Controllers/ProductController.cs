﻿using E_Farm.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace E_Farm.Controllers
{
    public class ProductController : Controller
    {
        DataEntities db = new DataEntities();

        // GET: Product
        public ActionResult Index(string search, int? pageNo)
        {
            var lstCategory = db.Categorys.ToList();
            Session["Category"] = lstCategory;

            if (search == null)
            {
                search = "";
            }
            int pageSize = 8;
            int pageNumber = pageNo ?? 1;
            try
            {
                return View(db.Products.Include(x => x.Category).Where(x => x.Price == float.Parse(search) && x.Status == true).ToList().ToPagedList(pageNumber, pageSize));
            }
            catch (Exception)
            {
                return View(db.Products.Include(x => x.Category).Where(x => x.Name.Contains(search) && x.Status == true).ToList().ToPagedList(pageNumber, pageSize));               
            }
        }

        public ActionResult OrderByPriceDESC()
        {

            return null;
        }
        
    }
}