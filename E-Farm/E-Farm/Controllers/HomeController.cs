﻿using E_Farm.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace E_Farm.Controllers
{
    public class HomeController : Controller
    {
        private DataEntities db = new DataEntities();
        public ActionResult Home()
        {
            var top8Product = db.Database.SqlQuery<Product>("EXEC usp_GetTop8Product").ToList();
            return View(top8Product);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string user_password)
        {
            string password = user_password.ToMD5();
            var account = db.AccountCustomer.SingleOrDefault(x => x.Username == username && x.Password == password);
            if (account != null)
            {
                // Gán Session lưu thông tin đăng nhập
                Session["login"] = 1;
                Session["CustomerId"] = account.CustomerId;
                Session["CustomerName"] = account.Fullname;
                Session["CustomerPhone"] = account.Phone;
                Session["CustomerAddress"] = account.Address;
                return RedirectToAction("Home");
            }
            else
            {
                // Thông báo lỗi khi người dùng nhập không đúng
                TempData["msg"] = "<div class='alert-danger' style='margin-bottom:20px'>Tài khoản hoặc mật khẩu không hợp lệ</div>";
                return View();
            }
        }

        // Đăng xuất
        public ActionResult Logout()
        {
            // Xóa Session lưu thông tin đăng nhập
            Session["login"] = 0;
            Session["CustomerId"] = null;
            Session["CustomerName"] = null;
            Session["CustomerPhone"] = null;
            Session["CustomerAddress"] = null;
            return RedirectToAction("Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(string fullname, string gender, string address, string phone, string email, string username, string user_password)
        {
            var acc = new AccountCustomer();
            acc.Fullname = fullname;
            

            return View();
        }


    }
}