﻿using E_Farm.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E_Farm.Controllers
{
    public class ShoppingCartController : Controller
    {
        DataEntities db = new DataEntities();

        // View giỏ hàng
        public ActionResult Index()
        {
            ViewBag.PayId = new SelectList(db.Payments, "PayId", "Name");

            ViewBag.TranId = new SelectList(db.Transports, "TranId", "Name");

            return View();
        }


        public ActionResult AddToCart(int id, int quantity)
        {
            var productSeleted = db.Products.Find(id);
            if (productSeleted != null)
            {
               
                // Check số lượng trong kho
                if (productSeleted.Quantity < quantity)
                {
                    return Content("Số lượng bạn mua vượt quá trong kho");
                }
                else
                {
                    
                    var orderProduct = new Basket()
                    {
                        ProductId = id,
                        Name = productSeleted.Name,
                        Price = float.Parse(((productSeleted.PriceDiscount > 0) ? productSeleted.PriceDiscount : productSeleted.Price).ToString()),
                        Picture = productSeleted.Picture,
                        Quantity = quantity,
                        TotalPrice = float.Parse(((productSeleted.PriceDiscount > 0) ? productSeleted.PriceDiscount : productSeleted.Price).ToString()) * quantity
                    };

                    var lstBasket = (List<Basket>) Session["Basket"];

                    var checkExist = lstBasket.SingleOrDefault(x => x.ProductId == id);
                    if (checkExist != null)
                    {
                        checkExist.Quantity += quantity;
                        checkExist.TotalPrice = checkExist.Price * checkExist.Quantity;
                    }
                    else
                    {
                        lstBasket.Add(orderProduct);
                    }

                    var totalQuantity = lstBasket.Sum(x => x.Quantity);
                    Session["TotalQuantity"] = totalQuantity;
                    var totalPrice = lstBasket.Sum(x => x.TotalPrice);
                    Session["TotalPrice"] = totalPrice;
                }

            }
            return null;
        }

        public ActionResult RemoveProduct(int id)
        {
            var checkExistProduct = db.Products.Find(id);
            if (checkExistProduct != null)
            {
                var lstBasket = (List<Basket>)Session["Basket"];
                var productRomoved = lstBasket.SingleOrDefault(x => x.ProductId == id);
                if (productRomoved != null)
                {
                    // remove sản phẩm khỏi giỏ hàng
                    lstBasket.Remove(productRomoved);

                    var totalQuantity = lstBasket.Sum(x => x.Quantity);
                    Session["TotalQuantity"] = totalQuantity;
                    var totalPrice = lstBasket.Sum(x => x.TotalPrice);
                    Session["TotalPrice"] = totalPrice;
                }

            }
            return null;
        }

        public ActionResult UpdateProduct(int id, int quantity)
        {

            var lstBasket = (List<Basket>)Session["Basket"];
            var productChecked = db.Products.Find(id);
            var productUpdated = lstBasket.SingleOrDefault(x => x.ProductId == id);
            if (productUpdated != null)
            {
                
                if (productChecked.Quantity < quantity)
                {
                    return Content("Số lượng bạn mua vượt quá trong kho");
                }

                // update số lượng và giá trong giỏ hàng
                productUpdated.Quantity = quantity;
                productUpdated.TotalPrice = productUpdated.Price * productUpdated.Quantity;

                // Gán session
                var totalQuantity = lstBasket.Sum(x => x.Quantity);
                Session["TotalQuantity"] = totalQuantity;
                var totalPrice = lstBasket.Sum(x => x.TotalPrice);
                Session["TotalPrice"] = totalPrice;
            }
            return null;
        }

        public ActionResult Order(int PaymentId, int TransportsId)
        {

            var CustomerId = Session["CustomerId"];
            var TotalPrice = Session["TotalPrice"];


            if (CustomerId != null)
            {
                // add vào order
                Order order = new Order();
                Guid guid = Guid.NewGuid();
                order.OrderId = guid.ToString();
                order.CustomerId = CustomerId.ToString();
                order.TotalPrice = float.Parse(TotalPrice.ToString());
                order.PayId = PaymentId;
                order.TranId = TransportsId;
                order.CreateDate = DateTime.Now;
                order.Status = false;
                db.Orders.Add(order);

                var lstBasket = (List<Basket>)Session["Basket"];
                var orderId = order.OrderId;

                // 
                foreach (var item in lstBasket)
                {
                    // add từng sản phẩm vào order detail
                    Guid guid1 = Guid.NewGuid();
                    OrderDetail orderDetail = new OrderDetail();
                    orderDetail.OrderDetailId = guid1.ToString();
                    orderDetail.ProductId = item.ProductId;
                    orderDetail.OrderId = orderId;
                    orderDetail.Quantity = item.Quantity;
                    orderDetail.TotalPrice = item.TotalPrice;
                    db.OrderDetails.Add(orderDetail);

                    // update số lượng
                    var productUpdatedQuantity = db.Products.Find(orderDetail.ProductId);
                    if (productUpdatedQuantity != null)
                    {
                        productUpdatedQuantity.ProductId = orderDetail.ProductId;
                        productUpdatedQuantity.Quantity -= orderDetail.Quantity;
                        db.Entry(productUpdatedQuantity).State = EntityState.Modified;
                    }

                }

                int total = db.SaveChanges();
                if (total > 0)
                {
                    Session["Basket"] = new List<Basket>();
                    Session["TotalPrice"] = 0;
                    Session["TotalQuantity"] = 0;
                    return Content("Đặt hàng thành công");
                }
                else
                {
                    return Content("Đặt hàng không thành công");
                }

            }
            return null;
        }

    }
}

