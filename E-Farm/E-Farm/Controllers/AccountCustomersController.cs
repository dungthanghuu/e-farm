﻿using E_Farm.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace E_Farm.Controllers
{
    public class AccountCustomersController : Controller
    {
        private DataEntities db = new DataEntities();

        public ActionResult Imfor(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            AccountCustomer accountCustomer = db.AccountCustomer.Find(id);
            if (accountCustomer == null)
            {
                return HttpNotFound();
            }
            return View(accountCustomer);
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(AccountCustomer accountCustomer, HttpPostedFileBase UploadImgAccCus, string ConfirmPassword)
        {
            var check = db.AccountCustomer.SingleOrDefault(x => x.Username == accountCustomer.Username);
            var checkMail = db.AccountCustomer.SingleOrDefault(x => x.Email == accountCustomer.Email);

            if (checkMail != null)
            {
                TempData["errMessages1"] = "<div class='alert alert-danger'><strong>Cảnh báo! </strong>Email đã tồn tại.</div>";
                return View(accountCustomer);
            }
            if (check != null)
            {
                TempData["errMessage"] = "<div class='alert alert-danger'><strong>Cảnh báo! </strong>Tên đăng nhập đã tồn tại.</div>";
                return View(accountCustomer);
            }
            if (accountCustomer.Password != ConfirmPassword)
            {
                TempData["errMessages1"] = "<div class='alert alert-danger'><strong>Cảnh báo! </strong>Mật khẩu không trùng nhau</div>";
                return View(accountCustomer);
            }
            if (ModelState.IsValid)
            {
                if (UploadImgAccCus != null)
                {
                    UploadImgAccCus.SaveAs(Server.MapPath("~/Areas/Admin/Content/imgAccCustomer/") + UploadImgAccCus.FileName);
                    accountCustomer.Picture = "/Areas/Admin/Content/imgAccCustomer/" + UploadImgAccCus.FileName;
                }
                Guid guid = Guid.NewGuid();
                accountCustomer.CustomerId = guid.ToString();
                accountCustomer.CreateDate = @DateTime.Now;
                accountCustomer.Password = accountCustomer.Password.ToMD5();
                db.AccountCustomer.Add(accountCustomer);
                db.SaveChanges();
                return RedirectToAction("Login", "Home");
            }

            return View(accountCustomer);
        }

        public ActionResult ChangeImfor(string id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            AccountCustomer accountCustomer = db.AccountCustomer.Find(id);
            if (accountCustomer == null)
            {
                return HttpNotFound();
            }
            return View(accountCustomer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeImfor(AccountCustomer accountCustomer, HttpPostedFileBase UploadImgAccCus, string newPassword, string confirmNewPassword)
        {
            if (newPassword != confirmNewPassword)
            {
                TempData["errMessages1"] = "<div class='alert alert-danger'><strong>Cảnh báo! </strong>Mật khẩu không trùng nhau</div>";
                return View(accountCustomer);
            }

            if (ModelState.IsValid)
            {
                if (UploadImgAccCus != null)
                {
                    UploadImgAccCus.SaveAs(Server.MapPath("~/Areas/Admin/Content/imgAccCustomer/") + UploadImgAccCus.FileName);
                    accountCustomer.Picture = "/Areas/Admin/Content/imgAccCustomer/" + UploadImgAccCus.FileName;
                }
                if (newPassword == "" && confirmNewPassword == "")
                {
                    accountCustomer.Password = accountCustomer.Password;
                    db.Entry(accountCustomer).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Imfor", new { id = accountCustomer.CustomerId });
                }
                else
                {
                    accountCustomer.Password = confirmNewPassword.ToMD5();
                    db.Entry(accountCustomer).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Imfor", new { id = accountCustomer.CustomerId });
                }
            }
            return View(accountCustomer);
        }
        
    }
}