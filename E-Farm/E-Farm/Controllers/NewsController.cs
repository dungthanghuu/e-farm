﻿using E_Farm.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace E_Farm.Controllers
{
    public class NewsController : Controller
    {
        DataEntities db = new DataEntities();

        // GET: News
        public ActionResult Index()
        {
            var top8Product = db.Database.SqlQuery<Product>("EXEC usp_GetTop8Product").ToList();
            Session["Top8Product"] = top8Product;
            return View(db.New.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.New.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }
    }
}